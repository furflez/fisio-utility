package com.br.amaral.fisioutility.dao.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;

import com.br.amaral.fisioutility.dao.BaseDAO;
import com.br.amaral.fisioutility.dao.TableBuilder;
import com.br.amaral.fisioutility.vo.Avaliador;

public class AvaliadorDao extends BaseDAO {

	public AvaliadorDao(Context ctx) {
		super(ctx);
	}

	public static final String TABELA = "AVALIADOR";

	public static final String IDAVALIADOR = "IDAVALIADOR";
	public static final String NOME = "NOME";
	public static final String NUMEROREGISTRO = "NUMEROREGISTRO";

	public static final String CREATE_TABLE = defineTable();

	private static String defineTable() {
		TableBuilder tb = new TableBuilder(TABELA);
		try {
			tb.setPrimaryKey(IDAVALIADOR, tb.INTEGER, true);
			tb.addColuna(NOME, tb.TEXT, true);
			tb.addColuna(NUMEROREGISTRO, tb.TEXT, false);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return tb.toString();
	}

	public long save(Avaliador avaliador) {

		ContentValues values = classeToValues(avaliador);
		return inserir(TABELA, values);
	}

	public long saveAll(List<Avaliador> avaliadorList) {
		long result = 0;
		for (Avaliador avaliador : avaliadorList) {
			ContentValues values = classeToValues(avaliador);
			result = inserir(TABELA, values);
		}
		return result;
	}

	public Avaliador loadById(Integer avaliadorId) {
		Cursor c = consultar(TABELA, IDAVALIADOR, String.valueOf(avaliadorId));
		Avaliador avaliador = cursorToClasse(c);
		c.close();
		return avaliador;
	}

	public static ContentValues classeToValues(Avaliador avaliador) {
		ContentValues values = new ContentValues();
		values.put(IDAVALIADOR, avaliador.getIdAvaliador());
		values.put(NOME, avaliador.getNome());
		values.put(NUMEROREGISTRO, avaliador.getNumeroRegistro());
		
		return values;
	}

	public static Avaliador cursorToClasse(Cursor c) {
		if (c == null || c.getCount() < 1) {
			return null;
		}

		Avaliador avaliador = new Avaliador();
		avaliador.setIdAvaliador(c.getInt(c.getColumnIndex(IDAVALIADOR)));
		avaliador.setNome(c.getString(c.getColumnIndex(NOME)));
		avaliador.setNumeroRegistro(c.getString(c.getColumnIndex(NUMEROREGISTRO)));
		return avaliador;
	}

	public long update(Avaliador avaliador) {
		ContentValues values = classeToValues(avaliador);
		return atualizar(TABELA, values, new String[] { IDAVALIADOR },
				new String[] { String.valueOf(avaliador.getIdAvaliador()) });
	}

	public boolean delete(Avaliador avaliador) {
		return remover(TABELA, IDAVALIADOR, avaliador.getIdAvaliador());
	}

	public List<Avaliador> getAll() {
		List<Avaliador> avaliadorList = new ArrayList<Avaliador>();

		Cursor mCursor = mDb.query(TABELA, null, null, null, null, null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		while (!mCursor.isAfterLast()) {
			Avaliador avaliador = cursorToClasse(mCursor);
			avaliadorList.add(avaliador);
			mCursor.moveToNext();
		}
		mCursor.close();
		return avaliadorList;
	}

	public long count() {
		return DatabaseUtils.queryNumEntries(mDb, TABELA);
	}

}