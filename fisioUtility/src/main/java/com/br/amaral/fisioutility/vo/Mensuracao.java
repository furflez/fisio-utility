package com.br.amaral.fisioutility.vo;

public class Mensuracao {

	private Integer idMensuracao;
	private Integer idAvaliacao;
	private String movimento;
	private String membro;
	private double angulo;
	private String caminhoImagem;
	private String observacao;

	public Integer getIdMensuracao() {
		return idMensuracao;
	}

	public void setIdMensuracao(Integer idMensuracao) {
		this.idMensuracao = idMensuracao;
	}

	public Integer getIdAvaliacao() {
		return idAvaliacao;
	}

	public void setIdAvaliacao(Integer idAvaliacao) {
		this.idAvaliacao = idAvaliacao;
	}

	public String getMovimento() {
		return movimento;
	}

	public void setMovimento(String movimento) {
		this.movimento = movimento;
	}

	public String getMembro() {
		return membro;
	}

	public void setMembro(String membro) {
		this.membro = membro;
	}

	public double getAngulo() {
		return angulo;
	}

	public void setAngulo(double angulo) {
		this.angulo = angulo;
	}

	public String getCaminhoImagem() {
		return caminhoImagem;
	}

	public void setCaminhoImagem(String caminhoImagem) {
		this.caminhoImagem = caminhoImagem;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

}
