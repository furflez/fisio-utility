package com.br.amaral.fisioutility.view;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.br.amaral.fisioutility.R;
import com.br.amaral.fisioutility.dao.impl.AvaliacaoDao;
import com.br.amaral.fisioutility.dao.impl.AvaliadorDao;
import com.br.amaral.fisioutility.dao.impl.MensuracaoDao;
import com.br.amaral.fisioutility.dao.impl.PacienteDao;
import com.br.amaral.fisioutility.vo.Avaliacao;
import com.br.amaral.fisioutility.vo.Avaliador;
import com.br.amaral.fisioutility.vo.Mensuracao;
import com.br.amaral.fisioutility.vo.Paciente;

public class RegistrarAvaliacaoActivity extends Activity {
	Activity activity = this;
	Bundle bundle;

	PacienteDao pacienteDao;
	AvaliadorDao avaliadorDao;
	AvaliacaoDao avaliacaoDao;
	MensuracaoDao mensuracaoDao;

	EditText editTextData;
	EditText editTextGrau;
	EditText editTextObservaoces;

	AutoCompleteTextView editTextMembro;
	AutoCompleteTextView editTextMovimento;

	Spinner spinnerPacientes;
	Spinner spinnerAvaliadores;

	Button buttonAdicionarNovoPaciente;
	Button buttonCancelar;
	Button buttonSalvar;

	List<Paciente> pacienteList;
	List<Avaliador> avaliadorList;
	SharedPreferences preferences;

	String[] membros;
	String[] movimentos;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		initialize();

		buttonCancelar.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
		buttonSalvar.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				salvar();
			}
		});

	}

	private void initialize(){
		preferences = getSharedPreferences("preferences", 1);
		setContentView(R.layout.activity_registraravaliacao);
		bundle = getIntent().getExtras();
		spinnerPacientes = (Spinner) findViewById(R.id.spinnerPacientes);
		spinnerAvaliadores = (Spinner) findViewById(R.id.spinnerAvaliador);

		editTextData = (EditText) findViewById(R.id.editTextData);
		editTextGrau = (EditText) findViewById(R.id.editTextGrauAdm);
		editTextMembro = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextViewMembro);
		editTextMovimento = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextViewMovimento);
		editTextObservaoces = (EditText) findViewById(R.id.editTextObservacoes);
		buttonCancelar = (Button) findViewById(R.id.buttonCancelar);
		buttonSalvar = (Button) findViewById(R.id.buttonSalvar);

		loadData();

		pacienteList = pacienteDao.getAll();
		avaliadorList = avaliadorDao.getAll();

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		editTextData.setText(dateFormat.format(new Date()));
		editTextGrau.setText(bundle.getDouble("angulo")+"");
		spinnerPacientes.setAdapter(new ArrayAdapter<Paciente>(activity, android.R.layout.simple_list_item_1, pacienteList));
		spinnerAvaliadores.setAdapter(new ArrayAdapter<Avaliador>(activity, android.R.layout.simple_list_item_1, avaliadorList));

		membros = preferences.getString("membros", "").split(";");
		movimentos = preferences.getString("movimentos", "").split(";");

		editTextMembro.setAdapter(new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, membros));
		editTextMovimento.setAdapter(new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, movimentos));



	}

	private void loadData() {
		pacienteDao = new PacienteDao(activity);
		pacienteDao.open();

		avaliadorDao = new AvaliadorDao(activity);
		avaliadorDao.open();

		avaliacaoDao = new AvaliacaoDao(activity);
		avaliacaoDao.open();

		mensuracaoDao = new MensuracaoDao(activity);
		mensuracaoDao.open();

	}

	private void salvar(){
		boolean ok = false;

		if(editTextGrau.length() == 0){
			
			editTextGrau.setError("O campo grau precisa ser preenchido");
			
		}else if(editTextMembro.length() == 0){
			
			editTextMembro.setError("O campo membro precisa ser preenchido");
			
		}else if(editTextMovimento.length() == 0){
			
			editTextMovimento.setError("O campo movimento precisa ser preenchido");
			
		} if(pacienteList.size() == 0){
			
			Toast.makeText(activity, "� necess�rio um paciente, cadastre um novo apertando o bot�o \"+\"", Toast.LENGTH_LONG).show();
			
		}else{
			ok = true;
		}

		if(ok){
			Avaliacao avaliacao = new Avaliacao();
			avaliacao.setData(new Date());
			avaliacao.setIdAvaliador(((Avaliador)spinnerAvaliadores.getSelectedItem()).getIdAvaliador());
			avaliacao.setIdPaciente(((Paciente)spinnerPacientes.getSelectedItem()).getIdPaciente());
			Integer idAvaliacao = (int) avaliacaoDao.save(avaliacao);
			
			Mensuracao mensuracao = new Mensuracao();
			mensuracao.setIdAvaliacao(idAvaliacao);
			mensuracao.setCaminhoImagem(bundle.getString("caminhoImagem"));
			mensuracao.setMembro(editTextMembro.getText().toString());
			mensuracao.setMovimento(editTextMovimento.getText().toString());
			mensuracao.setAngulo(Double.parseDouble(editTextGrau.getText().toString().replace(",", ".")));
			mensuracao.setObservacao(editTextObservaoces.getText().toString());
			
			
			if(mensuracaoDao.save(mensuracao) == -1){
				Toast.makeText(activity, "erro ao salvar os dados", Toast.LENGTH_SHORT).show();
			}else{
				Toast.makeText(activity, "Avalia��o Registrada!", Toast.LENGTH_SHORT).show();
				String membs = "";
				for (String membro : membros) {
					membs = membs + membro + ";";
				}
				String movs = "";
				for (String movimento : movimentos) {
					movs = movs + movimento + ";";
				}
				boolean igual = false;
				for (String membro : membros) {
					if(membro.equalsIgnoreCase(mensuracao.getMembro())){
						igual = true;
						break;
					}
				}
				if(!igual || movs.length() == 0){
					membs = membs + mensuracao.getMembro()+";";
				}
				
				igual = false;
				for (String movimento : movimentos) {
					if(movimento.equalsIgnoreCase(mensuracao.getMovimento())){
						igual = true;
						break;
					}
				}
				if(!igual || movs.length() == 0){
					movs = movs + mensuracao.getMovimento()+";";
				}
				
				Editor editor = preferences.edit();
				editor.putString("membros", membs);
				editor.putString("movimentos", movs);
				
				editor.commit();
				
				
				activity.finish();
				FinalizarGoniometriaActivity.activity.finish();
				GoniometriaActivity.activity.finish();
			}
			
			
		}
	}
}
