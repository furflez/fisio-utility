package com.br.amaral.fisioutility.view.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.br.amaral.fisioutility.R;
import com.br.amaral.fisioutility.vo.Paciente;

public class PacienteListAdapter extends ArrayAdapter<Paciente> {

	List<Paciente> pacientes;

	public PacienteListAdapter(Context context, List<Paciente> pacientes) {
		super(context, 0, pacientes);
		this.pacientes = pacientes;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = LayoutInflater.from(getContext()).inflate(R.layout.listadapter_paciente, parent, false);
		}
		TextView textViewNome = (TextView) convertView.findViewById(R.id.textViewNome);
		TextView textViewUltimaAvaliacao = (TextView) convertView.findViewById(R.id.textViewUltimaAvaliacao);
		
		textViewNome.setText(pacientes.get(position).getNome());
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		textViewUltimaAvaliacao.setText(dateFormat.format(pacientes.get(position).getDataUltimaAvaliacao()));

		return convertView;
	}

}
