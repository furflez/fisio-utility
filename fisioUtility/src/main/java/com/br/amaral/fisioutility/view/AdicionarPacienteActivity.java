package com.br.amaral.fisioutility.view;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.br.amaral.fisioutility.R;
import com.br.amaral.fisioutility.dao.impl.PacienteDao;
import com.br.amaral.fisioutility.util.Mascara;
import com.br.amaral.fisioutility.vo.Paciente;

public class AdicionarPacienteActivity extends Activity {

	Activity activity;

	PacienteDao pacienteDao;
	EditText editTextNome;
	EditText editTextDataNacimento;
	RadioButton radioButtonMasculino;

	Button buttonSalvar;
	Button buttonCancelar;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initialize();

		buttonSalvar.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				salvarNovoPaciente();
			}
		});
		buttonCancelar.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
	}

	private void initialize(){
		activity = this;

		setContentView(R.layout.activity_cadastropaciente);

		editTextNome = (EditText) findViewById(R.id.editTextNome);
		editTextDataNacimento = (EditText) findViewById(R.id.editTextDataNascimento);
		TextWatcher dataMask = Mascara.insert("##/##/####", editTextDataNacimento);
		editTextDataNacimento.addTextChangedListener(dataMask);
		radioButtonMasculino = (RadioButton) findViewById(R.id.radioMasculino);
		buttonSalvar = (Button) findViewById(R.id.buttonSalvar);
		buttonCancelar = (Button) findViewById(R.id.buttonCancelar);
		loadData();
	}

	private void loadData() {
		pacienteDao = new PacienteDao(activity);
		pacienteDao.open();
	}

	private void salvarNovoPaciente(){
		boolean ok = false;
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Paciente paciente = new Paciente();
		paciente.setNome(editTextNome.getText().toString());
		paciente.setSexo(radioButtonMasculino.isChecked() ? "M" : "F");

		if(paciente.getNome().length() == 0){
			editTextNome.setError("O nome precisa ser preenchido");
		}else{
			ok = true;
		}
		if(editTextDataNacimento.getText().toString().length() == 0){
			editTextNome.setError("A data de nascimento precisa ser preenchida");
		}else{
			ok = true;
		}
		try {
			paciente.setDataNascimento(dateFormat.parse(editTextDataNacimento.getText().toString()));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(ok){
			if(pacienteDao.save(paciente) != -1){
				Toast.makeText(activity, "Paciente registrado!", Toast.LENGTH_SHORT).show();
				activity.finish();
				PacienteListActivity.updateScreen();
			}else{
				Toast.makeText(activity, "Erro ao registrar paciente", Toast.LENGTH_SHORT).show();
			}
		}
	}
}
