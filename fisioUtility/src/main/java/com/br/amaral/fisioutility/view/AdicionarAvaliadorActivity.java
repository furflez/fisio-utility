package com.br.amaral.fisioutility.view;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.br.amaral.fisioutility.R;
import com.br.amaral.fisioutility.dao.impl.AvaliadorDao;
import com.br.amaral.fisioutility.vo.Avaliador;

public class AdicionarAvaliadorActivity extends Activity {

	Activity activity;

	AvaliadorDao avaliadorDao;
	EditText editTextNome;
	EditText editTextNumeroRegistro;

	Button buttonSalvar;
	Button buttonCancelar;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initialize();

		buttonSalvar.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				salvarNovoAvaliador();
			}
		});
		buttonCancelar.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
	}

	private void initialize(){
		activity = this;

		setContentView(R.layout.activity_cadastroavaliador);

		editTextNome = (EditText) findViewById(R.id.editTextNome);
		editTextNumeroRegistro = (EditText) findViewById(R.id.editTextNumeroRegistro);
		buttonSalvar = (Button) findViewById(R.id.buttonSalvar);
		buttonCancelar = (Button) findViewById(R.id.buttonCancelar);
		loadData();
	}

	private void loadData() {
		avaliadorDao = new AvaliadorDao(activity);
		avaliadorDao.open();
	}

	private void salvarNovoAvaliador(){
		boolean ok = false;
		Avaliador avaliador = new Avaliador();
		avaliador.setNome(editTextNome.getText().toString());
		avaliador.setNumeroRegistro(editTextNumeroRegistro.getText().toString());
		if(avaliador.getNome().length() == 0){
			editTextNome.setError("O nome precisa ser preenchido");
		}else{
			ok = true;
		}

		if(ok){
			if(avaliadorDao.save(avaliador) != -1){
				Toast.makeText(activity, "Avaliador registrado!", Toast.LENGTH_SHORT).show();
				activity.finish();
				AvaliadoresListActivity.updateScreen();
			}else{
				Toast.makeText(activity, "Erro ao registrar avaliador", Toast.LENGTH_SHORT).show();
			}
		}
	}
}
