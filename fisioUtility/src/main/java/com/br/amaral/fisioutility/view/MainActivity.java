package com.br.amaral.fisioutility.view;

import java.util.Date;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.br.amaral.fisioutility.R;
import com.br.amaral.fisioutility.dao.impl.AvaliadorDao;
import com.br.amaral.fisioutility.dao.impl.PacienteDao;
import com.br.amaral.fisioutility.vo.Avaliador;
import com.br.amaral.fisioutility.vo.Paciente;

public class MainActivity extends Activity {
	Activity activity;
	
	Button buttonIniciar;
	Button buttonPacientes;
	Button buttonAvaliacoes;
	Button buttonAvaliador;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initialize();
		
		buttonIniciar.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(activity, GoniometriaActivity.class));
			}
		});
		buttonPacientes.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(activity, PacienteListActivity.class));
			}
		});
		
		buttonAvaliacoes.setOnClickListener(new View.OnClickListener() {
		
			@Override
			public void onClick(View v) {
				startActivity(new Intent(activity,AvaliacoesListActivity.class));
			}
		});
		buttonAvaliador.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(activity,AvaliadoresListActivity.class));
			}
		});
	}

	private void initialize() {
		activity = this;
		setContentView(R.layout.activity_main);
		buttonIniciar = (Button) findViewById(R.id.buttonIniciar);
		buttonPacientes = (Button) findViewById(R.id.buttonPacientes);
		buttonAvaliacoes = (Button) findViewById(R.id.buttonAvaliacoes);
		buttonAvaliador = (Button) findViewById(R.id.buttonAvaliador);
		
		AvaliadorDao avaliadorDao = new AvaliadorDao(activity);
		avaliadorDao.open();

		Avaliador avaliador = new Avaliador();
		avaliador.setNome("AVALIADOR PADR�O");
		avaliador.setNumeroRegistro("0");
		avaliador.setIdAvaliador(0);		
		
		if(avaliadorDao.update(avaliador) == 0){
			avaliadorDao.save(avaliador);
		}

	}
}
