package com.br.amaral.fisioutility.view;

import java.util.ArrayList;
import java.util.List;

import com.br.amaral.fisioutility.R;
import com.br.amaral.fisioutility.dao.impl.AvaliadorDao;
import com.br.amaral.fisioutility.view.adapter.AvaliadorListAdapter;
import com.br.amaral.fisioutility.vo.Avaliador;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

public class AvaliadoresListActivity extends Activity {

	private static Activity activity;
	private static ListView listViewAvaliadores;
	private Button buttonAdicionar;
	
	private static List<Avaliador> avaliadores;
	private static AvaliadorDao AvaliadorDao;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initialize();
		
		buttonAdicionar.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(activity, AdicionarAvaliadorActivity.class));
			}
		});
	}


	private void initialize(){
		activity = this;
	
		setContentView(R.layout.activity_avaliadoreslist);
		listViewAvaliadores = (ListView) findViewById(R.id.listViewAvaliadores);
		buttonAdicionar = (Button) findViewById(R.id.buttonAdicionar);
		
		avaliadores = new ArrayList<Avaliador>();
		loadData();
		updateScreen();
		
	}
	
	
	private void loadData() {
		AvaliadorDao = new AvaliadorDao(activity);
		AvaliadorDao.open();
		
		
	}
	public static void updateScreen(){
		avaliadores = AvaliadorDao.getAll();
		listViewAvaliadores.setAdapter(new AvaliadorListAdapter(activity, avaliadores));
	}
}
