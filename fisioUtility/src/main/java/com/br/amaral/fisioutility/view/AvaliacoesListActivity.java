package com.br.amaral.fisioutility.view;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.br.amaral.fisioutility.R;
import com.br.amaral.fisioutility.dao.impl.AvaliacaoDao;
import com.br.amaral.fisioutility.view.adapter.AvaliacaoListAdapter;
import com.br.amaral.fisioutility.vo.Avaliacao;

public class AvaliacoesListActivity extends Activity {

	private static Activity activity;
	private static ListView listViewAvaliacoes;
	
	private static List<Avaliacao> avaliacoes;
	private static AvaliacaoDao AvaliacaoDao;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initialize();
		
	}

	private void initialize(){
		activity = this;
	
		setContentView(R.layout.activity_avaliacoeslist);
		listViewAvaliacoes = (ListView) findViewById(R.id.listViewAvaliacoes);
		
		listViewAvaliacoes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Bundle bundle = new Bundle();
				bundle.putInt("idAvaliacao", avaliacoes.get(arg2).getIdAvaliacao());
				startActivity(new Intent(activity, AvaliacaoViewActivity.class).putExtras(bundle));
				
			}
		});
		
		avaliacoes = new ArrayList<Avaliacao>();
		loadData();
		updateScreen();
		
	}
	
	
	private void loadData() {
		AvaliacaoDao = new AvaliacaoDao(activity);
		AvaliacaoDao.open();
		
		
	}
	public static void updateScreen(){
		avaliacoes = AvaliacaoDao.getAll();
		listViewAvaliacoes.setAdapter(new AvaliacaoListAdapter(activity, avaliacoes));
	}
}
