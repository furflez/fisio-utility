package com.br.amaral.fisioutility.vo;

public class Avaliador {

	private Integer idAvaliador;
	private String nome;
	private String numeroRegistro;

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return nome;
	}
	
	public Integer getIdAvaliador() {
		return idAvaliador;
	}

	public void setIdAvaliador(Integer idAvaliador) {
		this.idAvaliador = idAvaliador;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNumeroRegistro() {
		return numeroRegistro;
	}

	public void setNumeroRegistro(String numeroRegistro) {
		this.numeroRegistro = numeroRegistro;
	}

}
