package com.br.amaral.fisioutility.view.adapter;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.br.amaral.fisioutility.R;
import com.br.amaral.fisioutility.dao.impl.AvaliadorDao;
import com.br.amaral.fisioutility.dao.impl.MensuracaoDao;
import com.br.amaral.fisioutility.dao.impl.PacienteDao;
import com.br.amaral.fisioutility.vo.Avaliacao;
import com.br.amaral.fisioutility.vo.Mensuracao;
import com.br.amaral.fisioutility.vo.Paciente;

public class AvaliacaoListAdapter extends ArrayAdapter<Avaliacao> {

	List<Avaliacao> avaliacoes;

	public AvaliacaoListAdapter(Context context, List<Avaliacao> avaliacoes) {
		super(context, 0, avaliacoes);
		this.avaliacoes = avaliacoes;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = LayoutInflater.from(getContext()).inflate(R.layout.listadapter_avaliacao, parent, false);
		}
		TextView textViewPaciente = (TextView) convertView.findViewById(R.id.textViewPaciente);
		TextView textViewData = (TextView) convertView.findViewById(R.id.TextViewData);
		TextView textViewAvaliador = (TextView) convertView.findViewById(R.id.textViewAvaliador);
		
		MensuracaoDao mensuracaoDao = new MensuracaoDao(getContext());
		mensuracaoDao.open();
		Mensuracao mensuracao = mensuracaoDao.loadByIdAvaliacao(avaliacoes.get(position).getIdAvaliacao());
		File arquivoImagem = new File(mensuracao.getCaminhoImagem());
		Bitmap bitmapImagem = BitmapFactory.decodeFile(arquivoImagem.getAbsolutePath());
		ImageView imageViewAvaliacao = (ImageView) convertView.findViewById(R.id.imageViewAvaliacao);
		imageViewAvaliacao.setImageBitmap(bitmapImagem);
		PacienteDao pacienteDao = new PacienteDao(getContext());
		textViewPaciente.setText(pacienteDao.loadById(avaliacoes.get(position).getIdPaciente()).getNome());
		AvaliadorDao avaliadorDao = new AvaliadorDao(getContext());
		textViewAvaliador.setText(avaliadorDao.loadById(avaliacoes.get(position).getIdAvaliador()).getNome());
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		textViewData.setText(dateFormat.format(avaliacoes.get(position).getData()));

		return convertView;
	}

}
