package com.br.amaral.fisioutility.view;


import java.io.File;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.br.amaral.fisioutility.R;
import com.br.amaral.fisioutility.util.DeteccaoBolhaCor;
import com.br.amaral.fisioutility.util.Utilidades;

public class GoniometriaActivity extends Activity implements OnTouchListener, CvCameraViewListener2 {
	private static final String TAG = "FisioUtility";

	private boolean corSelecionada = false;
	private Mat mRgba;
	private Scalar bolhaCorRgba;
	private Scalar bolhaCorHsv;
	private DeteccaoBolhaCor mDetector;
	private Scalar COR_CONTORNO;
	private Scalar COR_LINHA_SELECIONADA;
	private Button buttonCapturar;

	private double angulo;
	public static Activity activity;

	private static Point point1, point2, pointToque;

	boolean stage2 = false;




	private CameraBridgeViewBase mOpenCvCameraView;

	private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
		@Override
		public void onManagerConnected(int status) {
			switch (status) {
			case LoaderCallbackInterface.SUCCESS:
			{
				mOpenCvCameraView.enableView();
				mOpenCvCameraView.setOnTouchListener(GoniometriaActivity.this);
			} break;
			default:
			{
				super.onManagerConnected(status);
			} break;
			}
		}
	};

	public GoniometriaActivity() {
		Log.i(TAG, "Instantiated new " + this.getClass());
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		activity = this;
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.activity_goniometria);

		mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.color_blob_detection_activity_surface_view);
		mOpenCvCameraView.setCvCameraViewListener(this);
		buttonCapturar = (Button) findViewById(R.id.buttonCapture);
		buttonCapturar.setEnabled(false);
		buttonCapturar.setOnClickListener(buttonCapturarClick());
	}

	private OnClickListener buttonCapturarClick(){
		return new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				corSelecionada = true;
				if(stage2){
					File path = new File(Environment.getExternalStorageDirectory().toString()+ "/fisioUtility/");
					if(!path.exists()){
						path.mkdir();
					}
					SimpleDateFormat simpleDateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
					String nomeArquivo = simpleDateFormat.format(new Date()) + ".png"; 
					File file = new File(path, nomeArquivo);
					Imgproc.cvtColor(mRgba, mRgba, Imgproc.COLOR_RGBA2BGRA);
					boolean b = Highgui.imwrite(file.toString(), mRgba);
					//						
					Toast.makeText(getApplicationContext(), b+ " "+file.getAbsolutePath(), Toast.LENGTH_LONG).show();
					Bundle bundle = new Bundle();
					bundle.putString("caminhoImagem", file.getAbsolutePath());
					bundle.putDouble("angulo", angulo);
					startActivity(new Intent(activity, FinalizarGoniometriaActivity.class).putExtras(bundle));
				}else{
					stage2 = true;
				}
			}
		};
	}

	@Override
	public void onPause()
	{
		super.onPause();
		if (mOpenCvCameraView != null)
			mOpenCvCameraView.disableView();
	}

	@Override
	public void onResume()
	{
		super.onResume();
		OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_3, this, mLoaderCallback);
	}

	public void onDestroy() {
		super.onDestroy();
		if (mOpenCvCameraView != null)
			mOpenCvCameraView.disableView();
	}

	public void onCameraViewStarted(int width, int height) {
		mRgba = new Mat(height, width, CvType.CV_8UC4);
		mDetector = new DeteccaoBolhaCor();
		bolhaCorRgba = new Scalar(255);
		bolhaCorHsv = new Scalar(255);
		COR_CONTORNO = new Scalar(255,0,0,255);
		COR_LINHA_SELECIONADA = new Scalar(0,255,0,255);
	}

	public void onCameraViewStopped() {
		mRgba.release();
	}

	public boolean onTouch(View v, MotionEvent event) {
		int cols = mRgba.cols();
		int rows = mRgba.rows();

		int desolcamentoX = (mOpenCvCameraView.getWidth() - cols) / 2;
		int deslocamentoY= (mOpenCvCameraView.getHeight() - rows) / 2;

		int x = (int)event.getX() - desolcamentoX;
		int y = (int)event.getY() - deslocamentoY;

		pointToque = new Point(x, y);

		if ((x < 0) || (y < 0) || (x > cols) || (y > rows)) return false;

		Rect retanguloTocado = new Rect();

		retanguloTocado.x = (x>4) ? x-4 : 0;
		retanguloTocado.y = (y>4) ? y-4 : 0;

		retanguloTocado.width = (x+4 < cols) ? x + 4 - retanguloTocado.x : cols - retanguloTocado.x;
		retanguloTocado.height = (y+4 < rows) ? y + 4 - retanguloTocado.y : rows - retanguloTocado.y;

		Mat regiaoTocadaRgba= mRgba.submat(retanguloTocado);

		Mat regiaoTocadaHsv = new Mat();
		Imgproc.cvtColor(regiaoTocadaRgba, regiaoTocadaHsv, Imgproc.COLOR_RGB2HSV_FULL);

		// Calculate average color of touched region
		bolhaCorHsv = Core.sumElems(regiaoTocadaHsv);
		int pointCount = retanguloTocado.width*retanguloTocado.height;
		for (int i = 0; i < bolhaCorHsv.val.length; i++)
			bolhaCorHsv.val[i] /= pointCount;

		bolhaCorRgba = converScalarHsv2Rgba(bolhaCorHsv);

		mDetector.setHsvColor(bolhaCorHsv);


		corSelecionada = true;

		regiaoTocadaRgba.release();
		regiaoTocadaHsv.release();

		return false; // don't need subsequent touch events
	}

	public Mat onCameraFrame(CvCameraViewFrame inputFrame) {


		mRgba = inputFrame.rgba();

		if (corSelecionada) {

			Point p1, p2;
			mDetector.process(mRgba);
			final List<MatOfPoint> contours = mDetector.getContours();
			Imgproc.drawContours(mRgba, contours, -1, COR_CONTORNO);

			if(contours.size() == 2){
				ArrayList<Point> pointList = new ArrayList<Point>();
				Point[] points = null;

				for (MatOfPoint matOfPoint : contours) {

					points = matOfPoint.toArray();
					System.out.println(points);
					double avgx = 0;
					double avgy = 0;
					for (Point point : points) {
						avgx = avgx + point.x;
						avgy = avgy + point.y;
					}
					avgx = avgx/points.length;
					avgy = avgy/points.length;
					if(stage2){
						ArrayList<Point> pts = new ArrayList<Point>();
						pts.add(point1);
						pts.add(point2);

						for (Point point : pts) {
							if(
									(point.x - avgx) >= -30 &&
									(point.x - avgx) <= 30 &&
									(point.y - avgy) >= -30 &&
									(point.y - avgy) <= 30 )
							{
								avgx = point.x;	
								avgy = point.y;
								Core.circle(mRgba, point, 5, COR_CONTORNO);	
							}
						}

					}
					System.out.println(avgx +" ; "+avgy);
					Point point = new Point(avgx, avgy);
					pointList.add(point);
					Core.circle(mRgba, point, 5, COR_CONTORNO);
					Core.putText(mRgba, (int)point.x + ", "+(int)point.y, point, Core.FONT_HERSHEY_PLAIN, 1, COR_CONTORNO);

				}
				Core.line(mRgba, pointList.get(0), pointList.get(1), COR_CONTORNO, 1, Core.LINE_AA, 0);
				double x1 = pointList.get(0).x - pointToque.x;
				double x2 = pointList.get(1).x - pointToque.x;
				double y1 = pointList.get(1).y - pointToque.y;
				double y2 = pointList.get(1).y - pointToque.y;

				x1 = Math.abs(x1);
				x2 = Math.abs(x2);
				y1 = Math.abs(y1);
				x1 = Math.abs(x1);
				if(x2 <= x1 && y2 <= y1 ){
					//aqui nesta parte s�o comparados os pontos para verificar o ponto mais pr�ximo do ponto de toque, 
					//o mais pr�ximo ser� o primeiro da lista.
					Point pontoTemporario1, pontoTemporario2;

					pontoTemporario1 = pointList.get(0);
					pontoTemporario2 = pointList.get(1);

					pointList.set(0, pontoTemporario2);
					pointList.set(1, pontoTemporario1);
				}

				if(!stage2){
					point1 = pointList.get(0);
					point2 = pointList.get(1);
				}else{
					p1 = new Point(point1.x, point1.y);
					p2 = new Point(point2.x, point2.y);

					double d = Math.toDegrees(Utilidades.degreecalc(point1, point2, pointList.get(0), pointList.get(1)));

					d = d * -1;
					DecimalFormat df = new DecimalFormat("#,###.00");  
					angulo = Double.parseDouble(df.format(d).replace(",", "."));  
					point1 = p1;
					point2 = p2;

					Core.putText(mRgba, angulo+"", new Point(50, 50), Core.FONT_HERSHEY_SIMPLEX, 1, COR_CONTORNO);

				}



			}
			if(stage2){
				Core.line(mRgba, point1, point2, COR_LINHA_SELECIONADA, 1, Core.LINE_AA, 0);
				Core.circle(mRgba, point1, 5, COR_LINHA_SELECIONADA);	
				Core.circle(mRgba, point2, 5, COR_LINHA_SELECIONADA);	
			}
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					buttonCapturar.setEnabled(contours.size() == 2 ? true : false);

				}
			});

		}

		return mRgba;
	}

	private Scalar converScalarHsv2Rgba(Scalar hsvColor) {
		Mat pointMatRgba = new Mat();
		Mat pointMatHsv = new Mat(1, 1, CvType.CV_8UC3, hsvColor);
		Imgproc.cvtColor(pointMatHsv, pointMatRgba, Imgproc.COLOR_HSV2RGB_FULL, 4);

		return new Scalar(pointMatRgba.get(0, 0));
	}
}


