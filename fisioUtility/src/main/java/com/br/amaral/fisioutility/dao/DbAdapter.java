package com.br.amaral.fisioutility.dao;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.br.amaral.fisioutility.dao.impl.AvaliacaoDao;
import com.br.amaral.fisioutility.dao.impl.AvaliadorDao;
import com.br.amaral.fisioutility.dao.impl.MensuracaoDao;
import com.br.amaral.fisioutility.dao.impl.PacienteDao;

public class DbAdapter {

	private static final String TAG = "DbAdapter";
	private DatabaseHelper mDbHelper;
	private SQLiteDatabase mDb;

	private static final String DB_NAME = "fisioUtilityDatabase";
	private static final int DATABASE_VERSION = 1;

	private final Context mCtx;

	private static class DatabaseHelper extends SQLiteOpenHelper {

		@Override
		public void onOpen(SQLiteDatabase db) {
			super.onOpen(db);
			if (!db.isReadOnly()) {
				db.execSQL("PRAGMA foreign_keys=ON;");
			}
		}

		DatabaseHelper(Context context) {
			super(context, DB_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {

			db.execSQL(PacienteDao.CREATE_TABLE);
			db.execSQL(AvaliacaoDao.CREATE_TABLE);
			db.execSQL(AvaliadorDao.CREATE_TABLE);
			db.execSQL(MensuracaoDao.CREATE_TABLE);

			Log.w("DbAdapter", "DB criado com sucesso!");
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			Log.w(TAG, "Atualizando o banco de dados da vers�o " + oldVersion
					+ " para " + newVersion + ", todos os dados ser�o perdidos!");
			boolean success = true;

			if (newVersion > oldVersion) {
				for (int i = oldVersion ; i < newVersion ; ++i) {

					int nextVersion = i + 1;

					switch (nextVersion) {
					case 2:
						break;

					}

					if (!success) {
						Log.d(TAG, "Error updating database, reverting changes!");
						break;
					}
				}
				if (success) {
					Log.d(TAG, "Database updated successfully!");
				}
			}else {
				onCreate(db);
			}
		}
		

	}

	public DbAdapter(Context ctx) {
		this.mCtx = ctx;
	}

	public  SQLiteDatabase open() throws SQLException {
		mDbHelper = new DatabaseHelper(mCtx);
		mDb = mDbHelper.getWritableDatabase();
		return mDb;
	}

	public void close() {
		mDbHelper.close();
		mDb.close();
	}

}
