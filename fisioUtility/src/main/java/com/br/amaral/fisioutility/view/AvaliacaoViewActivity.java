package com.br.amaral.fisioutility.view;

import java.io.File;
import java.text.SimpleDateFormat;

import com.br.amaral.fisioutility.R;
import com.br.amaral.fisioutility.dao.impl.AvaliacaoDao;
import com.br.amaral.fisioutility.dao.impl.AvaliadorDao;
import com.br.amaral.fisioutility.dao.impl.MensuracaoDao;
import com.br.amaral.fisioutility.dao.impl.PacienteDao;
import com.br.amaral.fisioutility.vo.Avaliacao;
import com.br.amaral.fisioutility.vo.Avaliador;
import com.br.amaral.fisioutility.vo.Mensuracao;
import com.br.amaral.fisioutility.vo.Paciente;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class AvaliacaoViewActivity extends Activity {

	Activity activity = this;
	TextView textViewData;
	TextView textViewAvaliador;
	TextView textViewPaciente;
	TextView textViewMembro;
	TextView textViewMovimento;
	TextView textViewGrauAdm;
	TextView textViewObservacoes;
	ImageView imageViewImagem;
	
	AvaliacaoDao avaliacaoDao;
	AvaliadorDao avaliadorDao;
	PacienteDao pacienteDao;
	MensuracaoDao mensuracaoDao;
	
	Avaliacao avaliacao;
	Avaliador avaliador;
	Paciente paciente;
	Mensuracao mensuracao;
	
	Bundle bundle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initialize();
	}

	private void initialize(){
		setContentView(R.layout.activity_avaliacaoview);
		bundle = getIntent().getExtras();
		textViewData = (TextView) findViewById(R.id.textViewData);
		textViewAvaliador = (TextView) findViewById(R.id.textViewAvaliador);
		textViewPaciente = (TextView) findViewById(R.id.textViewPaciente);
		textViewMembro = (TextView) findViewById(R.id.textViewMembro);
		textViewMovimento = (TextView) findViewById(R.id.textViewMovimento);
		textViewGrauAdm = (TextView) findViewById(R.id.textViewGrauAdm);
		textViewObservacoes = (TextView) findViewById(R.id.textViewObservacoes);
		imageViewImagem = (ImageView) findViewById(R.id.imageViewAvaliacao);
		
		loadData();
		File arquivoImagem = new File(mensuracao.getCaminhoImagem());
		Bitmap bitmapImagem = BitmapFactory.decodeFile(arquivoImagem.getAbsolutePath());
		imageViewImagem.setImageBitmap(bitmapImagem);
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		textViewData.setText(dateFormat.format(avaliacao.getData()));
		textViewAvaliador.setText(avaliador.getNome());
		textViewPaciente.setText(paciente.getNome());
		textViewMembro.setText(mensuracao.getMembro());
		textViewMovimento.setText(mensuracao.getMovimento());
		textViewGrauAdm.setText(mensuracao.getAngulo()+"�");
		textViewObservacoes.setText(mensuracao.getObservacao());
	}

	private void loadData() {
		pacienteDao = new PacienteDao(activity);
		pacienteDao.open();
		avaliacaoDao = new AvaliacaoDao(activity);
		avaliacaoDao.open();
		avaliadorDao = new AvaliadorDao(activity);
		avaliadorDao.open();
		mensuracaoDao = new MensuracaoDao(activity);
		mensuracaoDao.open();
		
		avaliacao = avaliacaoDao.loadById(bundle.getInt("idAvaliacao"));
		paciente = pacienteDao.loadById(avaliacao.getIdPaciente());
		mensuracao = mensuracaoDao.loadByIdAvaliacao(avaliacao.getIdAvaliacao());
		avaliador = avaliadorDao.loadById(avaliacao.getIdAvaliador());
		
	}

}