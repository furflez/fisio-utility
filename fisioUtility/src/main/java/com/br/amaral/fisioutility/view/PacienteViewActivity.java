package com.br.amaral.fisioutility.view;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.br.amaral.fisioutility.R;
import com.br.amaral.fisioutility.dao.impl.AvaliacaoDao;
import com.br.amaral.fisioutility.dao.impl.PacienteDao;
import com.br.amaral.fisioutility.view.adapter.AvaliacaoListAdapter;
import com.br.amaral.fisioutility.vo.Avaliacao;
import com.br.amaral.fisioutility.vo.Paciente;

public class PacienteViewActivity extends Activity {

	Activity activity = this;

	Bundle bundle;

	TextView textViewPaciente;
	TextView textViewIdade;
	TextView textViewSexo;
	TextView textViewPrimeiraAvaliacao;
	TextView textViewUltimaAvaliacao;

	ListView listViewAvaliacoes;

	AvaliacaoDao avaliacaoDao;
	PacienteDao pacienteDao;

	List<Avaliacao> avaliacoes;
	Paciente paciente;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		initialize();
	}

	private void initialize() {

		setContentView(R.layout.activity_pacienteview);
		bundle = getIntent().getExtras();
		getData();

		textViewPaciente = (TextView) findViewById(R.id.textViewPaciente);
		textViewIdade = (TextView) findViewById(R.id.textViewIdade);
		textViewSexo = (TextView) findViewById(R.id.textViewSexo);
		textViewPrimeiraAvaliacao = (TextView) findViewById(R.id.textViewPrimeiraAvaliacao);
		textViewUltimaAvaliacao = (TextView) findViewById(R.id.textViewUltimaAvaliacao);
		listViewAvaliacoes = (ListView) findViewById(R.id.listViewAvaliacoes);

		update();
		
		listViewAvaliacoes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				bundle.putInt("idAvaliacao", avaliacoes.get(arg2).getIdAvaliacao());
				startActivity(new Intent(activity, AvaliacaoViewActivity.class).putExtras(bundle));
			}
		});

	}

	private void getData() {
		avaliacaoDao = new AvaliacaoDao(activity);
		pacienteDao = new PacienteDao(activity);
		avaliacaoDao.open();
		pacienteDao.open(); 

		paciente = pacienteDao.loadById(bundle.getInt("idPaciente"));
	}

	private void update(){
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		avaliacoes = avaliacaoDao.getAllByIdPaciente(paciente.getIdPaciente());
		listViewAvaliacoes.setAdapter(new AvaliacaoListAdapter(activity, avaliacoes));

		textViewPaciente.setText(paciente.getNome());
		textViewIdade.setText(String.valueOf(calcularIdade()));
		textViewSexo.setText(paciente.getSexo().equals("M") ? "Masculino" : "Feminino");
		textViewPrimeiraAvaliacao.setText(dateFormat.format(paciente.getDataPrimeiraAvaliacao()));
		textViewUltimaAvaliacao.setText(dateFormat.format(paciente.getDataUltimaAvaliacao()));
	}

	private int calcularIdade(){
		Calendar dataAtual = Calendar.getInstance();
		Calendar dataNascimento = Calendar.getInstance();
		dataNascimento.setTime(paciente.getDataNascimento());
		if (dataNascimento.after(dataAtual)) {
			throw new IllegalArgumentException("Data incorreta para o calculo");
		}
		int anoAtual = dataAtual.get(Calendar.YEAR);
		int anoNascimento = dataNascimento.get(Calendar.YEAR);
		int idade = anoAtual - anoNascimento;
		int mesAtual = dataAtual.get(Calendar.MONTH);
		int mesNascimento = dataNascimento.get(Calendar.MONTH);
		if (mesNascimento > mesAtual) {
			idade--;
		} else if (mesAtual == mesNascimento) {
			int diaAtual = dataAtual.get(Calendar.DAY_OF_MONTH);
			int diaNascimento = dataNascimento.get(Calendar.DAY_OF_MONTH);
			if (diaNascimento > diaAtual) {
				idade--;
			}
		}
		return idade;
	}

}
