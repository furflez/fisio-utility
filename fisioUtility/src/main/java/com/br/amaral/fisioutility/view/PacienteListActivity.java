package com.br.amaral.fisioutility.view;

import java.util.ArrayList;
import java.util.List;

import com.br.amaral.fisioutility.R;
import com.br.amaral.fisioutility.dao.impl.PacienteDao;
import com.br.amaral.fisioutility.view.adapter.PacienteListAdapter;
import com.br.amaral.fisioutility.vo.Paciente;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

public class PacienteListActivity extends Activity {

	private static Activity activity;
	private static ListView listViewPacientes;
	private Button buttonAdicionar;
	
	private static List<Paciente> pacientes;
	private static PacienteDao PacienteDao;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initialize();
		
		buttonAdicionar.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(activity, AdicionarPacienteActivity.class));
			}
		});
		
		listViewPacientes.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Bundle bundle = new Bundle();
				bundle.putInt("idPaciente", pacientes.get(arg2).getIdPaciente());
				startActivity(new Intent(activity, PacienteViewActivity.class).putExtras(bundle));
			}
		});
	}


	private void initialize(){
		activity = this;
	
		setContentView(R.layout.activity_pacienteslist);
		listViewPacientes = (ListView) findViewById(R.id.listViewPacientes);
		buttonAdicionar = (Button) findViewById(R.id.buttonAdicionar);
		
		pacientes = new ArrayList<Paciente>();
		loadData();
		updateScreen();
		
	}
	
	
	private void loadData() {
		PacienteDao = new PacienteDao(activity);
		PacienteDao.open();
		
		
	}
	public static void updateScreen(){
		pacientes = PacienteDao.getAll();
		listViewPacientes.setAdapter(new PacienteListAdapter(activity, pacientes));
	}
}
