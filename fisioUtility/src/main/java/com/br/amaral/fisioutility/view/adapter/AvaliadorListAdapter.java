package com.br.amaral.fisioutility.view.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.br.amaral.fisioutility.R;
import com.br.amaral.fisioutility.vo.Avaliador;

public class AvaliadorListAdapter extends ArrayAdapter<Avaliador> {

	List<Avaliador> avaliadores;

	public AvaliadorListAdapter(Context context, List<Avaliador> avaliadores) {
		super(context, 0, avaliadores);
		this.avaliadores = avaliadores;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = LayoutInflater.from(getContext()).inflate(R.layout.listadapter_avaliador, parent, false);
		}
		TextView textViewNome = (TextView) convertView.findViewById(R.id.textViewNome);
		TextView textViewNumerRegistro = (TextView) convertView.findViewById(R.id.textViewNumeroRegistro);
		
		textViewNome.setText(avaliadores.get(position).getNome());
		textViewNumerRegistro.setText(avaliadores.get(position).getNumeroRegistro());

		return convertView;
	}

}
