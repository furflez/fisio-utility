package com.br.amaral.fisioutility.util;

import org.opencv.core.Point;

public class Utilidades {

	public static double degreecalc(Point ponto1, Point ponto2, Point ponto3, Point ponto4){
		int aux1;
		int aux2;

		aux1 = (int) ponto1.x;
		aux2 = (int) ponto1.y;

		ponto1.x = 0;
		ponto1.y = 0;

		ponto2.x = ponto2.x - aux1;
		ponto2.y = ponto2.y - aux2;

		aux1 = (int) ponto3.x;
		aux2 = (int) ponto3.y;

		ponto3.x = 0;
		ponto3.y = 0;

		ponto4.x = ponto4.x - aux1;
		ponto4.y = ponto4.y - aux2;

		double angulo1 = Math.atan2(ponto1.x - ponto2.x,
				ponto1.y - ponto2.y);
		double angle2 = Math.atan2(ponto3.x - ponto4.x,
				ponto3.y - ponto4.y);

		return angulo1-angle2;
	}
	
	
}
