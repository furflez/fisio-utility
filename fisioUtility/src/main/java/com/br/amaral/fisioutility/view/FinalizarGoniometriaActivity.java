package com.br.amaral.fisioutility.view;

import java.io.File;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.br.amaral.fisioutility.R;

public class FinalizarGoniometriaActivity extends Activity {
	Bundle bundle;
	
	ImageView imageView;
	TextView textViewResultado;
	
	Button buttonRegistrar;
	Button buttonRepetir;
	Button buttonMenuPrincipal;
	
	File arquivoImagem;
	
	public static Activity activity;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		bundle = getIntent().getExtras();
		
		initialize();
		
		buttonRepetir.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				arquivoImagem.delete();
				startActivity(new Intent(activity, GoniometriaActivity.class));
				activity.finish();
			}
		});
		buttonMenuPrincipal.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				arquivoImagem.delete();
				activity.finish();
				GoniometriaActivity.activity.finish();
			}
		});
		buttonRegistrar.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(activity, RegistrarAvaliacaoActivity.class).putExtras(bundle));
			}
		});
	}
	
	private void initialize(){
		activity = this;
		setContentView(R.layout.activity_finalizargoniometria);
		arquivoImagem = new File(bundle.getString("caminhoImagem"));
		Bitmap bitmapImagem = BitmapFactory.decodeFile(arquivoImagem.getAbsolutePath());
		imageView = (ImageView) findViewById(R.id.imageViewFoto);
		textViewResultado = (TextView) findViewById(R.id.textViewResultado);
		buttonRegistrar = (Button) findViewById(R.id.buttonRegistrarAvaliacao);
		buttonRepetir = (Button) findViewById(R.id.buttonRepetir);
		buttonMenuPrincipal = (Button) findViewById(R.id.buttonVoltarMenuPrincipal);
		textViewResultado.setText("Resultado: "+bundle.getDouble("angulo")+"�");
		imageView.setImageBitmap(bitmapImagem);
		
		GoniometriaActivity.activity.finish();
		
	}
}
