package com.br.amaral.fisioutility.dao.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;

import com.br.amaral.fisioutility.dao.BaseDAO;
import com.br.amaral.fisioutility.dao.TableBuilder;
import com.br.amaral.fisioutility.vo.Mensuracao;

public class MensuracaoDao extends BaseDAO {

	public MensuracaoDao(Context ctx) {
		super(ctx);
	}

	public static final String TABELA = "MENSURACAO";

	public static final String IDMENSURACAO = "IDMENSURACAO";
	public static final String IDAVALIACAO = "IDAVALIACAO";
	public static final String MOVIMENTO = "MOVIMENTO";
	public static final String MEMBRO = "MEMBRO";
	public static final String ANGULO = "ANGULO";
	public static final String CAMINHOIMAGEM = "CAMINHOIMAGEM";
	public static final String OBSERVACAO = "OBSERVACAO";

	public static final String CREATE_TABLE = defineTable();

	private static String defineTable() {
		TableBuilder tb = new TableBuilder(TABELA);
		try {
			tb.setPrimaryKey(IDMENSURACAO, tb.INTEGER, true);
			tb.addColuna(IDAVALIACAO, tb.INTEGER, true);
			tb.addColuna(MOVIMENTO, tb.TEXT, true);
			tb.addColuna(MEMBRO, tb.TEXT, true);
			tb.addColuna(CAMINHOIMAGEM, tb.TEXT, true);
			tb.addColuna(OBSERVACAO, tb.TEXT, false);
			tb.addColuna(ANGULO, tb.REAL, true);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return tb.toString();
	}

	public long save(Mensuracao mensuracao) {

		ContentValues values = classeToValues(mensuracao);
		return inserir(TABELA, values);
	}

	public long saveAll(List<Mensuracao> mensuracaoList) {
		long result = 0;
		for (Mensuracao mensuracao : mensuracaoList) {
			ContentValues values = classeToValues(mensuracao);
			result = inserir(TABELA, values);
		}
		return result;
	}

	public Mensuracao loadById(Integer mensuracaoId) {
		Cursor c = consultar(TABELA, IDMENSURACAO, String.valueOf(mensuracaoId));
		Mensuracao mensuracao = cursorToClasse(c);
		c.close();
		return mensuracao;
	}
	public Mensuracao loadByIdAvaliacao(Integer avaliacaoId) {
		Cursor c = consultar(TABELA, IDAVALIACAO, String.valueOf(avaliacaoId));
		Mensuracao mensuracao = cursorToClasse(c);
		c.close();
		return mensuracao;
	}

	public static ContentValues classeToValues(Mensuracao mensuracao) {
		ContentValues values = new ContentValues();
		values.put(IDMENSURACAO, mensuracao.getIdMensuracao());
		values.put(IDAVALIACAO, mensuracao.getIdAvaliacao());
		values.put(MOVIMENTO, mensuracao.getMovimento());
		values.put(MEMBRO, mensuracao.getMembro());
		values.put(ANGULO, mensuracao.getAngulo());
		values.put(CAMINHOIMAGEM, mensuracao.getCaminhoImagem());
		values.put(OBSERVACAO, mensuracao.getObservacao());
		
		return values;
	}

	public static Mensuracao cursorToClasse(Cursor c) {
		if (c == null || c.getCount() < 1) {
			return null;
		}

		Mensuracao mensuracao = new Mensuracao();
		mensuracao.setIdMensuracao(c.getInt(c.getColumnIndex(IDMENSURACAO)));
		mensuracao.setIdAvaliacao(c.getInt(c.getColumnIndex(IDAVALIACAO)));
		mensuracao.setMovimento(c.getString(c.getColumnIndex(MOVIMENTO)));
		mensuracao.setMembro(c.getString(c.getColumnIndex(MEMBRO)));
		mensuracao.setAngulo(c.getDouble(c.getColumnIndex(ANGULO)));
		mensuracao.setCaminhoImagem(c.getString(c.getColumnIndex(CAMINHOIMAGEM)));
		mensuracao.setObservacao(c.getString(c.getColumnIndex(OBSERVACAO)));
		return mensuracao;
	}

	public long update(Mensuracao mensuracao) {
		ContentValues values = classeToValues(mensuracao);
		return atualizar(TABELA, values, new String[] { IDMENSURACAO },
				new String[] { String.valueOf(mensuracao.getIdMensuracao()) });
	}

	public boolean delete(Mensuracao mensuracao) {
		return remover(TABELA, IDMENSURACAO, mensuracao.getIdMensuracao());
	}

	public List<Mensuracao> getAll() {
		List<Mensuracao> mensuracaoList = new ArrayList<Mensuracao>();

		Cursor mCursor = mDb.query(TABELA, null, null, null, null, null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		while (!mCursor.isAfterLast()) {
			Mensuracao mensuracao = cursorToClasse(mCursor);
			mensuracaoList.add(mensuracao);
			mCursor.moveToNext();
		}
		mCursor.close();
		return mensuracaoList;
	}

	public long count() {
		return DatabaseUtils.queryNumEntries(mDb, TABELA);
	}

}