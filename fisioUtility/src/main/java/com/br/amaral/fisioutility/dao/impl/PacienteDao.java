package com.br.amaral.fisioutility.dao.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.preference.PreferenceActivity;

import com.br.amaral.fisioutility.dao.BaseDAO;
import com.br.amaral.fisioutility.dao.TableBuilder;
import com.br.amaral.fisioutility.vo.Paciente;

public class PacienteDao extends BaseDAO {

	public PacienteDao(Context ctx) {
		super(ctx);
	}

	public static final String TABELA = "PACIENTE";

	public static final String IDPACIENTE = "IDPACIENTE";
	public static final String NOME = "NOME";
	public static final String SEXO = "SEXO";
	public static final String DATANASCIMENTO = "DATANASCIMENTO";
	public static final String DATAPRIMAVALIACAO = "DATAPRIMAVALIACAO";
	public static final String DATAULTAVALIACAO = "DATAULTAVALIACAO";

	public static final String CREATE_TABLE = defineTable();

	private static String defineTable() {
		TableBuilder tb = new TableBuilder(TABELA);
		try {
			tb.setPrimaryKey(IDPACIENTE, tb.INTEGER, true);
			tb.addColuna(NOME, tb.TEXT, true);
			tb.addColuna(SEXO, tb.TEXT, true);
			tb.addColuna(DATANASCIMENTO, tb.TEXT, false);
			tb.addColuna(DATAPRIMAVALIACAO, tb.TEXT, false);
			tb.addColuna(DATAULTAVALIACAO, tb.TEXT, false);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return tb.toString();
	}

	public long save(Paciente paciente) {

		ContentValues values = classeToValues(paciente);
		return inserir(TABELA, values);
	}

	public long saveAll(List<Paciente> pacienteList) {
		long result = 0;
		for (Paciente paciente : pacienteList) {
			ContentValues values = classeToValues(paciente);
			result = inserir(TABELA, values);
		}
		return result;
	}

	public Paciente loadById(Integer pacienteId) {
		Cursor c = consultar(TABELA, IDPACIENTE, String.valueOf(pacienteId));
		Paciente paciente = cursorToClasse(c);
		c.close();
		return paciente;
	}

	public static ContentValues classeToValues(Paciente paciente) {
		ContentValues values = new ContentValues();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");

		values.put(IDPACIENTE, paciente.getIdPaciente());
		values.put(NOME, paciente.getNome());
		values.put(SEXO, paciente.getSexo());
		try {
			values.put(DATANASCIMENTO,
					dateFormat.format(paciente.getDataNascimento()));
		} catch (Exception e) {
			values.put(DATANASCIMENTO, dateFormat.format(new Date()));
		}
		try {
			values.put(DATANASCIMENTO,
					dateFormat.format(paciente.getDataPrimeiraAvaliacao()));
		} catch (Exception e) {
			values.put(DATAPRIMAVALIACAO, dateFormat.format(new Date()));
		}
		try {
			values.put(DATANASCIMENTO,
					dateFormat.format(paciente.getDataUltimaAvaliacao()));
		} catch (Exception e) {
			values.put(DATAULTAVALIACAO, dateFormat.format(new Date()));
		}

		return values;
	}

	public static Paciente cursorToClasse(Cursor c) {
		if (c == null || c.getCount() < 1) {
			return null;
		}
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");

		Paciente paciente = new Paciente();
		paciente.setIdPaciente(c.getInt(c.getColumnIndex(IDPACIENTE)));
		paciente.setNome(c.getString(c.getColumnIndex(NOME)));
		paciente.setSexo(c.getString(c.getColumnIndex(SEXO)));
		try {
			paciente.setDataNascimento(dateFormat.parse(c.getString(c
					.getColumnIndex(DATANASCIMENTO))));
		} catch (ParseException e) {
			paciente.setDataNascimento(new Date());
		}
		try {
			paciente.setDataPrimeiraAvaliacao(dateFormat.parse(c.getString(c
					.getColumnIndex(DATAPRIMAVALIACAO))));
		} catch (ParseException e) {
			paciente.setDataPrimeiraAvaliacao(new Date());
		}
		try {
			paciente.setDataUltimaAvaliacao(dateFormat.parse(c.getString(c
					.getColumnIndex(DATAULTAVALIACAO))));
		} catch (ParseException e) {
			paciente.setDataUltimaAvaliacao(new Date());
		}

		return paciente;
	}

	public long update(Paciente paciente) {
		ContentValues values = classeToValues(paciente);
		return atualizar(TABELA, values, new String[] { IDPACIENTE },
				new String[] { String.valueOf(paciente.getIdPaciente()) });
	}

	public boolean delete(Paciente paciente) {
		return remover(TABELA, IDPACIENTE, paciente.getIdPaciente());
	}

	public List<Paciente> getAll() {
		List<Paciente> pacienteList = new ArrayList<Paciente>();

		Cursor mCursor = mDb.query(TABELA, null, null, null, null, null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		while (!mCursor.isAfterLast()) {
			Paciente paciente = cursorToClasse(mCursor);
			pacienteList.add(paciente);
			mCursor.moveToNext();
		}
		mCursor.close();
		return pacienteList;
	}

	public long count() {
		return DatabaseUtils.queryNumEntries(mDb, TABELA);
	}

}