package com.br.amaral.fisioutility.dao.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;

import com.br.amaral.fisioutility.dao.BaseDAO;
import com.br.amaral.fisioutility.dao.TableBuilder;
import com.br.amaral.fisioutility.vo.Avaliacao;

public class AvaliacaoDao extends BaseDAO {

	public AvaliacaoDao(Context ctx) {
		super(ctx);
	}

	public static final String TABELA = "AVALIACAO";

	public static final String IDAVALIACAO = "IDAVALIACAO";
	public static final String IDAVALIADOR = "IDAVALIADOR";
	public static final String IDPACIENTE = "IDPACIENTE";
	public static final String DATA = "DATA";

	public static final String CREATE_TABLE = defineTable();

	private static String defineTable() {
		TableBuilder tb = new TableBuilder(TABELA);
		try {
			tb.setPrimaryKey(IDAVALIACAO, tb.INTEGER, true);
			tb.addColuna(IDAVALIADOR, tb.INTEGER, true);
			tb.addColuna(IDPACIENTE, tb.INTEGER, true);
			tb.addColuna(DATA, tb.TEXT, true);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return tb.toString();
	}

	public long save(Avaliacao avaliacao) {

		ContentValues values = classeToValues(avaliacao);
		return inserir(TABELA, values);
	}

	public long saveAll(List<Avaliacao> avaliacaoList) {
		long result = 0;
		for (Avaliacao avaliacao : avaliacaoList) {
			ContentValues values = classeToValues(avaliacao);
			result = inserir(TABELA, values);
		}
		return result;
	}

	public Avaliacao loadById(Integer idPaciente) {
		Cursor c = consultar(TABELA, IDAVALIACAO, String.valueOf(idPaciente));
		Avaliacao avaliacao = cursorToClasse(c);
		c.close();
		return avaliacao;
	}

	public static ContentValues classeToValues(Avaliacao avaliacao) {
		ContentValues values = new ContentValues();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		values.put(IDAVALIACAO, avaliacao.getIdAvaliacao());
		values.put(IDAVALIADOR, avaliacao.getIdAvaliador());
		values.put(IDPACIENTE, avaliacao.getIdPaciente());
		values.put(DATA, dateFormat.format(avaliacao.getData()));

		return values;
	}

	public static Avaliacao cursorToClasse(Cursor c) {
		if (c == null || c.getCount() < 1) {
			return null;
		}
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");

		Avaliacao avaliacao = new Avaliacao();
		avaliacao.setIdAvaliacao(c.getInt(c.getColumnIndex(IDAVALIACAO)));
		avaliacao.setIdAvaliador(c.getInt(c.getColumnIndex(IDAVALIADOR)));
		avaliacao.setIdPaciente(c.getInt(c.getColumnIndex(IDPACIENTE)));
		try {
			avaliacao.setData(dateFormat.parse(c.getString(c.getColumnIndex(DATA))));
		} catch (ParseException e) {
		}

		return avaliacao;
	}

	public long update(Avaliacao avaliacao) {
		ContentValues values = classeToValues(avaliacao);
		return atualizar(TABELA, values, new String[] { IDAVALIACAO },
				new String[] { String.valueOf(avaliacao.getIdAvaliacao()) });
	}

	public boolean delete(Avaliacao avaliacao) {
		return remover(TABELA, IDAVALIACAO, avaliacao.getIdAvaliacao());
	}

	public List<Avaliacao> getAll() {
		List<Avaliacao> avaliacaoList = new ArrayList<Avaliacao>();

		Cursor mCursor = mDb.query(TABELA, null, null, null, null, null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		while (!mCursor.isAfterLast()) {
			Avaliacao avaliacao = cursorToClasse(mCursor);
			avaliacaoList.add(avaliacao);
			mCursor.moveToNext();
		}
		mCursor.close();
		return avaliacaoList;
	}
	public List<Avaliacao> getAllByIdPaciente(Integer idPaciente) {
		List<Avaliacao> avaliacaoList = new ArrayList<Avaliacao>();

		Cursor mCursor = mDb.rawQuery("SELECT * FROM "+TABELA+" WHERE "+IDPACIENTE+" = "+idPaciente, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		while (!mCursor.isAfterLast()) {
			Avaliacao avaliacao = cursorToClasse(mCursor);
			avaliacaoList.add(avaliacao);
			mCursor.moveToNext();
		}
		mCursor.close();
		return avaliacaoList;
	}

	public long count() {
		return DatabaseUtils.queryNumEntries(mDb, TABELA);
	}

	
}