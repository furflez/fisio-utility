package com.br.amaral.fisioutility.vo;

import java.util.Date;

public class Paciente {

	private Integer idPaciente;
	private String nome;
	private String sexo;
	private Date dataNascimento;
	private Date dataPrimeiraAvaliacao;
	private Date dataUltimaavaliacao;

	@Override
	public String toString() {
		return nome;
	}
	
	public Integer getIdPaciente() {
		return idPaciente;
	}

	public void setIdPaciente(Integer idPaciente) {
		this.idPaciente = idPaciente;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Date getDataPrimeiraAvaliacao() {
		return dataPrimeiraAvaliacao;
	}

	public void setDataPrimeiraAvaliacao(Date dataPrimeiraAvaliacao) {
		this.dataPrimeiraAvaliacao = dataPrimeiraAvaliacao;
	}

	public Date getDataUltimaAvaliacao() {
		return dataUltimaavaliacao;
	}

	public void setDataUltimaAvaliacao(Date dataUltimaavaliacao) {
		this.dataUltimaavaliacao = dataUltimaavaliacao;
	}

}
